<?php

namespace FirstBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{

    
    public function indexAction()
    {

        return $this->render('@First/index/index.html.twig');
    }

    
    public function genericAction()
    {

        return $this->render('@First/index/generic.html.twig');
    }
    
    /**
     * @Route("/ajax_snippet_image_send", name="ajax_snippet_image_send")
     */
    public function ajaxSnippetImageSendAction(Request $request)
    {
	   
        $em = $this->container->get("doctrine.orm.default_entity_manager");
        $document = new Document();
        $media = $request->files->get('file');
        $document->setFile($media);
        $document->setPath($media->getPathName());
        $document->setName($media->getClientOriginalName());
        $document->upload();
        $em->persist($document);
        $em->flush();
        //infos sur le document envoyé
        var_dump($request->files->get('file'));die;
        return new JsonResponse(array('success' => true));
        return $this->render('@First/index/generic.html.twig', array('media' => $media) );
    }

}
